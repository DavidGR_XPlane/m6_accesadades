package m3uf1_llistesArrays;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class IntercanviMaximsMinims {

	public static void main(String[] args) {

		ArrayList<Integer> arr1 = omplirLlista();
		ArrayList<Integer> arr2 = omplirLlista();

		System.out.println("Llista 1: " + arr1);
		System.out.println("Llista 2: " + arr2);

		ArrayList<Integer> comuns = trobarComuns(arr1, arr2);

		System.out.println("Llista amb elements comuns: " + comuns);

		comuns = maxMin(comuns);

		System.out.println("Llista amb posició MAX i MIN intercanviada: " + comuns);

	}

	private static ArrayList<Integer> omplirLlista() {
		Random r = new Random();
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for (int i = 0; i < 10; i++) {
			arr.add(r.nextInt(1, 21));
		}
		return arr;
	}

	private static ArrayList<Integer> trobarComuns(ArrayList<Integer> arr1, ArrayList<Integer> arr2) {
		ArrayList<Integer> comuns = new ArrayList<Integer>();

		for (int i = 0; i < 10; i++) {
			int aux = arr1.get(i);
			for (int j = 0; j < 10; j++) {
				if (aux == arr2.get(j))
					comuns.add(aux);
			}
		}
		return comuns;
	}

	private static ArrayList<Integer> maxMin(ArrayList<Integer> comuns) {

		int max = Collections.max(comuns);
		int min = Collections.min(comuns);

		boolean fetMax = false;
		boolean fetMin = false;

		for (int i = 0; i < comuns.size(); i++) {
			if (comuns.get(i) == max && !fetMax) {
				comuns.add(i, min);
				comuns.remove(i + 1);
				fetMax = true;
			}
			if (comuns.get(i) == min && !fetMin) {
				comuns.add(i, max);
				comuns.remove(i + 1);
				fetMin = true;
			}
		}
		return comuns;
	}
}
