package m3uf1_llistesArrays;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class CorreusMrAlbareda {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		ArrayList<String> nomsarr = new ArrayList<String>();
		ArrayList<String> cognomsarr = new ArrayList<String>();
		ArrayList<String> correusFinalsarr = new ArrayList<String>();
		int casos = sc.nextInt();
		sc.nextLine();

		// noms
		String noms;
		noms = sc.nextLine();
		String[] n = new String[casos];
		n = noms.split(" ");
		for (int i = 0; i < casos; i++) {
			nomsarr.add(n[i]);
		}

		// cognoms
		String cognoms;
		cognoms = sc.nextLine();
		String[] c = new String[casos];
		c = cognoms.split(" ");

		String any = sc.nextLine();

		// comprobem si hi ha algun cognom repetit
		boolean fet = false;

		for (int i = 0; i < c.length; i++) {
			String cognomAuxiliar = c[i];
			for (int j = 0; j < c.length; j++) {
				if (c[j].equals(cognomAuxiliar) && !fet) {
					c[j] += any.substring(2, 4);
					fet = true;
				}
			}

		}

		for (int i = 0; i < casos; i++) {
			cognomsarr.add(c[i]);
		}

		for (int i = 0; i < casos; i++) {
			String inicial = nomsarr.get(i).toLowerCase().charAt(0) + "";
			String cognom = cognomsarr.get(i).toLowerCase();
			String correufinal = inicial + cognom + "@ies-sabadell.cat";
			correusFinalsarr.add(correufinal);
		}

		Collections.sort(correusFinalsarr);

		// comprobacio de si el marc està a la llista
		for (int i = 0; i < correusFinalsarr.size(); i++) {
			if (correusFinalsarr.get(i).equals("malbareda@ies-sabadell.cat")) {
				String marc = correusFinalsarr.get(i);
				correusFinalsarr.remove(i);
				correusFinalsarr.add(0, marc);
			}
		}

		System.out.println(correusFinalsarr);

		sc.close();
	}

}
