package m3uf1_llistesArrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class ParellsSenarsVocalsConsonants {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int vocalsEliminades = 0;
		StringBuilder string = new StringBuilder();
		string.append(sc.nextLine());
		ArrayList<Character> vocals = new ArrayList<Character>(
				Arrays.asList('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'));
		for (int i = 0; i < string.length(); i++) {
			// parell
			if (i % 2 == 0) {
				if (vocals.contains(string.charAt(i))) {
					string.deleteCharAt(i);
					i--;
					vocalsEliminades++;
				}
				// senar
			} else {
				string.setCharAt(i, '*');
			}
		}
		sc.close();
		if (string.length() == 0)
			System.out.println("buida");
		else
			System.out.println(string);
		System.out.println(vocalsEliminades + " vocals eliminades");

	}

}
