package m3uf1_bucles;

import java.util.Scanner;

public class XifratgeCesar {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		boolean beingUsed = true;
		int canvis = 0;
		String frase = "";

		while (beingUsed) {
			System.out.println();
			System.out.println("1- Introduir una frase o paraula");
			System.out.println("2- Modificar la frase introduida");
			System.out.println("3- Eliminar la frase introduida");
			System.out.println("4- Xifratge light");
			System.out.println("5- Sortir");

			int option = sc.nextInt();
			sc.nextLine();
			switch (option) {
			// introduir fase
			case 1:
				frase = sc.nextLine().toUpperCase();
				break;
			// concatenar a frase
			case 2:
				frase += sc.nextLine().toUpperCase();
				break;
			// eliminar frase
			case 3:
				frase = "";
				break;
			// xifratge light
			case 4:
				if (frase.length() == 0) {
					System.out.println("BUIDA!");
					beingUsed = false;
				} else {
					for (int i = 0; i < frase.length(); i++) {
						if (frase.charAt(i) == 'A' || frase.charAt(i) == 'E' || frase.charAt(i) == 'C'
								|| frase.charAt(i) == 'P') {
							canvis++;
						}
					}
					System.out.println();
					System.out.println("frase: " + frase);
					System.out.println("Nombre de canvis: " + canvis + " lletres");
					System.out.println();
				}
				break;
			// sortir del programa
			case 5:
				System.out.println("Sortint...");
				beingUsed = false;
				break;

			default:
				System.out.println("Ave, Caesar, morituri te salutant");
				break;
			}
		}
		sc.close();

	}
}
