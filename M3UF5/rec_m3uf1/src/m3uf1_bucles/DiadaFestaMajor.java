package m3uf1_bucles;

import java.util.Scanner;

public class DiadaFestaMajor {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int casos = sc.nextInt();
		boolean error = false;

		int num;

		int boomers = 0;
		int joves = 0;

		int intentatsA = 0;
		int descarregatsA = 0;
		int intentatsB = 0;
		int descarregatsB = 0;

		for (int i = 0; i < casos; i++) {

			for (int j = 1; j < 8 + 1; j++) {
				num = sc.nextInt();
				switch (j) {
				case 1:
					intentatsA += num;
					boomers += num;
					break;
				case 2:
					descarregatsA += num;
					boomers += num;
					break;
				case 3:
					intentatsA += num;
					joves += num;
					break;
				case 4:
					descarregatsA += num;
					joves += num;
					break;
				case 5:
					intentatsB += num;
					boomers += num;
					break;
				case 6:
					descarregatsB += num;
					boomers += num;
					break;
				case 7:
					intentatsB += num;
					joves += num;
					break;
				case 8:
					descarregatsB += num;
					joves += num;
					break;

				}
				if (descarregatsA > intentatsA || descarregatsB > intentatsB) {
					error = true;
				}
			}
		}

		if (error) {
			System.out.println("ERROR");
		} else {
			if (descarregatsA > descarregatsB) {
				System.out.println("A");
			} else {
				System.out.println("B");
			}

			if (joves > boomers) {
				System.out.println("JOVES");
			} else {
				System.out.println("BOOMERS");
			}

		}
		sc.close();
	}
}
