package m3uf1_bucles;

import java.util.Scanner;

public class ParellsSenarsMagic {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int num = 0;
		boolean magia = false;
		int sumaSenar = 0;
		int sumaParell = 0;

		while (num != -1) {
			num = sc.nextInt();
			// parell
			if (num % 2 == 0) {
				sumaParell += num;
				// senar
			} else {
				sumaSenar += num;
			}

			if (num == 73) {
				magia = true;
			}

		}
		if (magia) {
			System.out.println("MAGIA");
		} else {
			if (sumaSenar > sumaParell) {
				System.out.println("SENARS");
			} else {
				System.out.println("PARELLS");
			}
		}
		sc.close();
	}
}
