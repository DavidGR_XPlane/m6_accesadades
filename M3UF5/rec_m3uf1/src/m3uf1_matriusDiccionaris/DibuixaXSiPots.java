package m3uf1_matriusDiccionaris;

import java.util.Scanner;

public class DibuixaXSiPots {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		boolean success = true;

		int fil = sc.nextInt();
		int col = sc.nextInt();

		char[][] taulell = new char[fil][col];

		int midaX = sc.nextInt();

		int filX = sc.nextInt();
		int colX = sc.nextInt();

		FuncionsUtils.omplirTaulellAmbPunts(taulell);
		success = posarX(taulell, midaX, filX, colX);
		if (success)
			FuncionsUtils.printarTaulellChar(taulell);
		sc.close();

	}

	/* no va */
	private static boolean posarX(char[][] taulell, int midaX, int filX, int colX) {
		taulell[filX][colX] = 'X';
		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell[0].length; j++) {
				if (i == filX && j == colX) {
					for (int k = 0; k < midaX; k++) {
						if (i - k < taulell.length && j - k < taulell[0].length && i + k < taulell.length
								&& j + k < taulell[0].length && i - k < taulell.length && j + k < taulell[0].length
								&& i + k < taulell.length && j - k < taulell[0].length) {
							taulell[i - k][j - k] = 'X';
							taulell[i + k][j + k] = 'X';
							taulell[i - k][j + k] = 'X';
							taulell[i + k][j - k] = 'X';
							midaX--;
						} else {
							System.out.println("NO HI CAP");
							return false;
						}
					}
				}
			}
		}
		return true;
	}
}
