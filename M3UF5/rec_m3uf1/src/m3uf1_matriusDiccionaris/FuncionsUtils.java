package m3uf1_matriusDiccionaris;

public class FuncionsUtils {


	public static void omplirTaulellAmbPunts(char[][] taulell) {
		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell[0].length; j++) {
				taulell[i][j] = '.';
			}
		}
	}
	
	public static void printarTaulellChar(char[][] taulell) {
		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell[0].length; j++) {
				System.out.print(taulell[i][j] + " ");
			}
			System.out.println();
		}
	}
	
}
