package m3uf1_matriusDiccionaris;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class RobarIUsar {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		LinkedHashMap<String, Integer> diccionari = new LinkedHashMap<>();

		String[] accio = sc.nextLine().split(" ");

		while (!accio[0].equals("FI")) {
			switch (accio[0]) {
			case "ROBAR":
				if (diccionari.containsKey(accio[1])) {
					if (diccionari.get(accio[1]) < 5) {
						diccionari.put(accio[1], diccionari.get(accio[1]) + 1);
					}
				} else {
					diccionari.put(accio[1], 1);
				}
				break;
			case "USAR":
				if (diccionari.containsKey(accio[1])) {
					diccionari.put(accio[1], diccionari.get(accio[1]) - 1);
					if (diccionari.get(accio[1]) <= 0) {
						diccionari.put(accio[1], 0);
					}
				} else {
					System.out.println("no pots utilitzar "+ accio[1] + ", no el tens al inventari!");
				}

				break;
			}
			accio = sc.nextLine().split(" ");
		}
		System.out.println(diccionari);
		comprobarPleBuit(diccionari);

		sc.close();

	}

	private static void comprobarPleBuit(LinkedHashMap<String, Integer> diccionari) {
		int ple = 0;
		int buit = 0;
		for (Map.Entry<String, Integer> entry : diccionari.entrySet()) {
			int val = entry.getValue();
			if (val == 0) {
				buit++;
			} else if (val == 5) {
				ple++;
			}
		}
		System.out.println("inventari ple: " + ple);
		System.out.println("inventari buit: " + buit);
	}
}
