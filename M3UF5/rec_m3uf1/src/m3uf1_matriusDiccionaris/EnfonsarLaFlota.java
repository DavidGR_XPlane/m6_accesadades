package m3uf1_matriusDiccionaris;

import java.util.Scanner;

public class EnfonsarLaFlota {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int dimensio = sc.nextInt();
		sc.nextLine();

		char[][] taulell = new char[dimensio][dimensio];
		FuncionsUtils.omplirTaulellAmbPunts(taulell);
		posarVaixell(taulell, sc);
		posarVaixell(taulell, sc);
		printarMatriu(taulell);
		comprovarTirada(taulell, sc);

		sc.close();
	}

	private static void comprovarTirada(char[][] taulell, Scanner sc) {
		String[] linia = sc.nextLine().split(" ");
		int fil = Integer.parseInt(linia[0]);
		int col = Integer.parseInt(linia[1]);

		if (taulell[fil][col] == 'x')
			System.out.println("TOCAT");
		else
			System.out.println("AIGUA");
	}

	private static void posarVaixell(char[][] taulell, Scanner sc) {
		String[] linia = sc.nextLine().split(" ");
		int fil = Integer.parseInt(linia[0]);
		int col = Integer.parseInt(linia[1]);
		taulell[fil][col] = 'x';
		switch (linia[2]) {
		case "V":
			for (int i = 1; i < 3; i++) {
				taulell[fil + i][col] = 'x';
			}
			break;
		case "H":
			for (int i = 1; i < 3; i++) {
				taulell[fil][col + i] = 'x';
			}
			break;
		}
	}

	public static void printarMatriu(char[][] mat) {
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat[0].length; j++) {
				System.out.print(mat[i][j]);
			}
			System.out.println();
		}
	}
}
