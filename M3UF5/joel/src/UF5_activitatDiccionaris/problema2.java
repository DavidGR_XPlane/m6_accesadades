package UF5_activitatDiccionaris;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

public class problema2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		int tornejos;
		int numGuanyador = 0;
		String txtTornejos;
		String trobar;
		// anyguanyador, nom anime
		HashMap<Integer, String> guanyadors = new HashMap<Integer, String>();
		// nom anime, cops guanyadors
		HashMap<String, Integer> comprobacions = new HashMap<String, Integer>();
		ArrayList<String> array = new ArrayList<String>();

		for (int i = 0; i < casos; i++) {
			tornejos = sc.nextInt();
			sc.nextLine();
			for (int j = 0; j < tornejos; j++) {
				txtTornejos = sc.nextLine();
				String[] arr = txtTornejos.split("-");
				guanyadors.put(Integer.parseInt(arr[0]), arr[1]);
				arr = null;
			}
			for (HashMap.Entry<Integer, String> entrada : guanyadors.entrySet()) {
				if (!comprobacions.containsKey(entrada.getValue())) {
					comprobacions.put(entrada.getValue(), 1);
				} else {
					comprobacions.put(entrada.getValue(), comprobacions.get(entrada.getValue()) + 1);
				}
			}
			for (HashMap.Entry<String, Integer> entrada : comprobacions.entrySet()) {
				array.add(entrada.getValue() + " " + entrada.getKey());
			}
			Collections.sort(array);
			Collections.reverse(array);
			for (int j = 0; j < array.size(); j++) {
				String[] arr2 = array.get(j).split(" ");
				System.out.println(arr2[1] + "-" + arr2[0]);
			}
		}
		sc.close();
	}
}