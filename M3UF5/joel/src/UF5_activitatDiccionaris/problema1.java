package UF5_activitatDiccionaris;

import java.util.HashMap;
import java.util.Scanner;

public class problema1 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		int tornejos;
		int numGuanyador = 0;
		String txtTornejos;
		String trobar;
		// anyguanyador, nom anime
		HashMap<Integer, String> guanyadors = new HashMap<Integer, String>();
		// nom anime, cops guanyadors
		HashMap<String, Integer> comprobacions = new HashMap<String, Integer>();

		for (int i = 0; i < casos; i++) {
			tornejos = sc.nextInt();
			sc.nextLine();
			for (int j = 0; j < tornejos - 1; j++) {
				txtTornejos = sc.nextLine();
				String[] arr = txtTornejos.split("-");
				guanyadors.put(Integer.parseInt(arr[0]), arr[1]);
				arr = null;
			}
			trobar = sc.nextLine();

			for (HashMap.Entry<Integer, String> entrada : guanyadors.entrySet()) {
				if (!comprobacions.containsKey(entrada.getValue())) {
					comprobacions.put(entrada.getValue(), 1);
				} else {
					comprobacions.put(entrada.getValue(), comprobacions.get(entrada.getValue()) + 1);
				}
			}
			if (comprobacions.containsKey(trobar)) {
				numGuanyador = comprobacions.get(trobar);
			}
			System.out.println(numGuanyador);
			numGuanyador = 0;
			guanyadors.clear();
			comprobacions.clear();
		}
		sc.close();
	}
}
