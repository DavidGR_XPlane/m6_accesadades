package UF5_joel;

import java.util.HashMap;
import java.util.Scanner;

public class ElPrimerQueArribiA5 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		String amicTxt = "";
		String guanyador = "";
		boolean hiHaGuanyador = false;
		HashMap<String, Integer> amics = new HashMap<String, Integer>();

		while (!amicTxt.equals("xxx")) {
			amicTxt = sc.nextLine();
			if (!amicTxt.equals("xxx")) {
				if (amics.containsKey(amicTxt)) {
					amics.put(amicTxt, amics.get(amicTxt) + 1);
				} else {
					amics.put(amicTxt, 1);
				}
			}
			for (HashMap.Entry<String, Integer> entrada : amics.entrySet()) {

				if (entrada.getValue() == 5) {
					guanyador = entrada.getKey();
				}
			}
		}
		for (HashMap.Entry<String, Integer> entrada : amics.entrySet()) {
			if (entrada.getValue() == 5) {
				hiHaGuanyador = true;
			}
		}
		if (hiHaGuanyador) {
			System.out.println(guanyador);
		} else {
			System.out.println("NO");
		}
	}
}
