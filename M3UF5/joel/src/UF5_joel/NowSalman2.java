package UF5_joel;

import java.util.LinkedHashMap;
import java.util.Scanner;

public class NowSalman2 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int casos = sc.nextInt();
		String txt;
		String trobar;
		LinkedHashMap<String, String> amics = new LinkedHashMap<String, String>();

		for (int i = 0; i < casos; i++) {
			int lincas = sc.nextInt(); // recollim linies del cas
			sc.nextLine();
			for (int j = 0; j < lincas - 1; j++) {
				txt = sc.nextLine(); // recollim els K i V
				String[] arr = txt.split(" "); // separem per espais
				if (amics.containsKey(arr[1])) {
					amics.put(arr[1], arr[0]);
				} else {
					amics.put(arr[0], arr[1]);
				}
			}
			trobar = sc.nextLine();
			System.out.println(amics.get(trobar));
			amics.clear();
		}
		sc.close();
	}
}