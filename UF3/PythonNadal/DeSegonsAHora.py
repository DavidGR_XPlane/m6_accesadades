segons = int(input())
hours = segons // 3600
minutes = (segons % 3600) // 60
seconds = segons % 60

print("{}:{}:{}".format(hours, minutes, seconds))
