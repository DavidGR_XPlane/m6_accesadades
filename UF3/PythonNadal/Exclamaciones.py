paraula = input()

while paraula != "FIN":
    exclamacionsInici = 0
    exclamacioFinal = 0
    for nabo in paraula:
        if nabo == "¡":
            exclamacionsInici += 1
        if nabo == "!":
            exclamacioFinal += 1
    if exclamacionsInici == exclamacioFinal:
        print("SI")
    else:
        print("NO")
    paraula = input()
