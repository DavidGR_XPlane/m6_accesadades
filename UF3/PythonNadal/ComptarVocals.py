casos = int(input())
for k in range(casos):
    a = 0
    e = 0
    i = 0
    o = 0
    u = 0
    linia = input()
    for char in linia:
        if char.lower() == "a":
            a += 1
        if char.lower() == "e":
            e += 1
        if char.lower() == "i":
            i += 1
        if char.lower() == "o":
            o += 1
        if char.lower() == "u":
            u += 1
    print("A: " + str(a) + "E: " + str(e) + "I: " + str(i) + "O: " + str(o) + "U: " + str(u))