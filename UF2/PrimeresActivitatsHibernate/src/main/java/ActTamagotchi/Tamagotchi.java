package ActTamagotchi;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Tamagotchi {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_Tamagotchi")
	private int id;
	@Column(name = "nom", length = 25)
	private String nom;
	@Column(name = "descripcio", length = 25)
	private String descripcio;
	@Column(name = "gana", nullable = false, columnDefinition = "double(6,2)")
	private double gana = 20;
	@Column(name = "viu")
	private boolean viu = true;
	@Column(name = "felicitat")
	private int felicitat = 50;
	@Column(name = "etapa")
	@Enumerated(EnumType.STRING)
	private Etapa etapa = Etapa.Baby;
	@Column(name = "dataNaixement")
	private LocalDateTime dataNaixement;
	@ManyToOne
	@JoinColumn(name = "id_Aliment")
	private Aliment aliment;
	@OneToOne(cascade = { CascadeType.ALL, CascadeType.REMOVE })
	@JoinColumn(name = "id_Joguina", unique = true)
	private Joguina joguina;
	/*
	 * @ManyToMany(fetch = FetchType.EAGER)
	 * 
	 * @JoinTable(name = "Amics", joinColumns = @JoinColumn(name = "id_Tamagotchi"),
	 * inverseJoinColumns = @JoinColumn(name = "id_Tamagotchi")) private
	 * Set<Tamagotchi> amics = new HashSet<Tamagotchi>();
	 */
	@ManyToMany(fetch = FetchType.EAGER)
	// @JoinTable(name = "Amics", joinColumns = @JoinColumn(name = "id_Tamagotchi"),
	// inverseJoinColumns = @JoinColumn(name = "id_Tamagotchi"))
	private List<Tamagotchi> amics = new ArrayList<Tamagotchi>();

	public Tamagotchi() {
		super();
	}

	public Tamagotchi(String nom, String descripcio) {
		super();
		this.nom = nom;
		this.descripcio = descripcio;
	}

	public Tamagotchi(String nom, String descripcio, Etapa etapa) {
		super();
		this.nom = nom;
		this.descripcio = descripcio;
		this.etapa = etapa;
	}

	public Tamagotchi(int id, String nom, String descripcio, double gana, boolean viu, int felicitat, Etapa etapa,
			LocalDateTime dataNaixement) {
		super();
		this.id = id;
		this.nom = nom;
		this.descripcio = descripcio;
		this.gana = gana;
		this.viu = viu;
		this.felicitat = felicitat;
		this.etapa = etapa;
		this.dataNaixement = dataNaixement;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public double getGana() {
		return gana;
	}

	public void setGana(double gana) {
		this.gana = gana;
	}

	public boolean isViu() {
		return viu;
	}

	public void setViu(boolean viu) {
		this.viu = viu;
	}

	public int getFelicitat() {
		return felicitat;
	}

	public void setFelicitat(int felicitat) {
		this.felicitat = felicitat;
	}

	public Etapa getEtapa() {
		return etapa;
	}

	public void setEtapa(Etapa etapa) {
		this.etapa = etapa;
	}

	public LocalDateTime getDataNaixement() {
		return dataNaixement;
	}

	public void setDataNaixement(LocalDateTime dataNaixement) {
		this.dataNaixement = dataNaixement;
	}

	public Aliment getAliment() {
		return aliment;
	}

	public void setAliment(Aliment aliment) {
		this.aliment = aliment;
	}

	public Joguina getJoguina() {
		return joguina;
	}

	public void setJoguina(Joguina joguina) {
		this.joguina = joguina;
	}

	public List<Tamagotchi> getAmics() {
		return amics;
	}

	public void setAmics(List<Tamagotchi> amics) {
		this.amics = amics;
	}

	public String toStringAliment() {
		return "Tamagotchi [id=" + id + ", idAliment=" + aliment.getId() + ", nomAliment=" + aliment.getNom() + "]";
	}

	@Override
	public String toString() {
		return "Tamagotchi [id=" + id + ", nom=" + nom + ", descripcio=" + descripcio + ", gana=" + gana + ", viu="
				+ viu + ", felicitat=" + felicitat + ", etapa=" + etapa + ", dataNaixement=" + dataNaixement
				+ ", aliment=" + aliment + ", joguina=" + joguina + ", primerAmic=" + amics.size() + "]";
	}

}
