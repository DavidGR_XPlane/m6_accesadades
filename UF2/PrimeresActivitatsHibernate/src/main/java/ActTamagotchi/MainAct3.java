package ActTamagotchi;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.service.ServiceRegistry;

import com.mysql.cj.jdbc.SuspendableXAConnection;

import ModelsDAO.DAOAliment;
import ModelsDAO.DAOJoguina;
import ModelsDAO.DAOTamagotchi;

public class MainAct3 {

	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;

	public static void main(String[] args) {

		// --------------------------ACTIVITAT 3-------------------------- //

		DAOTamagotchi daoTamagotchi = new DAOTamagotchi();
		DAOAliment daoAliment = new DAOAliment();
		DAOJoguina daoJoguina = new DAOJoguina();

		Tamagotchi tama1 = new Tamagotchi("Tamago1", "Descripcio1", Etapa.Baby);
		Tamagotchi tama2 = new Tamagotchi("Tamago2", "Descripcio2", Etapa.Baby);
		Tamagotchi tama3 = new Tamagotchi("Tamago3", "Descripcio3", Etapa.Baby);
		daoTamagotchi.save(tama1);
		daoTamagotchi.save(tama2);
		daoTamagotchi.save(tama3);

		Aliment aliment1 = new Aliment("Poma", "descPoma", 25.00);
		Aliment aliment2 = new Aliment("Pastís de carn", "descPastisDeCarn", 135.00);
		Aliment aliment3 = new Aliment("Pollastre", "descPollastre", 50.00);
		daoAliment.save(aliment1);
		daoAliment.save(aliment2);
		daoAliment.save(aliment3);

		Joguina joguina1 = new Joguina("Pilota", "descPilota", 3);
		Joguina joguina2 = new Joguina("Taula", "descTaula", 1);
		Joguina joguina3 = new Joguina("Hacha", "descHacha", 90);
		daoJoguina.save(joguina1);
		daoJoguina.save(joguina2);
		daoJoguina.save(joguina3);

		// ---------------------------- //
		// ASSIGNEM JOGUINES I FEM AMICS //
		// ---------------------------- //

		daoTamagotchi.equiparAliment(tama1, aliment1);
		daoTamagotchi.equiparJoguina(tama1, joguina3);
		daoTamagotchi.ferAmics(tama1, tama2);
		System.out.println();
		System.out.println("Amics del TAMA1");
		System.out.println(tama1.getAmics());
		System.out.println(); 
		System.out.println("Amics del TAMA2");
		System.out.println(tama2.getAmics());

		// ---------------------------- //
		// MODIFIQUEM VALORNUTRICIONAL I NIVELLDIVERSIO //
		// ---------------------------- //

		//

		daoAliment.tamagotchisAmbAquestAliment(aliment1);

		System.out.println("A menjar!");
		System.out.println("Tinc gana: " + tama1.getGana()); // pre menjar
		System.out.println();
		daoTamagotchi.menjar(tama1);
		System.out.println();
		System.out.println("Desprès de menjar tinc gana: " + tama1.getGana()); // post menjar

		// jugar

		System.out.println("A jugar!");
		System.out.println("Tinc felicitat ABANS de jugar: " + tama1.getFelicitat()); // pre jugar
		daoTamagotchi.jugar(tama1);
		System.out.println("Tinc felicitat DESPRÈS de jugar: " + tama1.getFelicitat()); // post jugar

	}

}
