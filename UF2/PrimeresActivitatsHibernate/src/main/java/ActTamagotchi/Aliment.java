package ActTamagotchi;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Aliment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_Aliment")
	private int id;
	@Column(name = "nom", length = 50, nullable = false)
	private String nom;
	@Column(name = "descripcio", length = 50, nullable = false)
	private String descripcio;
	@Column(name = "valorNutricional", columnDefinition = "double(6,2)")
	private double valorNutricional = 10.00;
	@OneToMany(mappedBy = "aliment", fetch = FetchType.EAGER) 
	private Set<Tamagotchi> tamagotchis = new HashSet<Tamagotchi>();

	public Aliment() {
		super();
	}

	public Aliment(String nom, String descripcio, double valorNutricional) {
		super();
		this.nom = nom;
		this.descripcio = descripcio;
		this.valorNutricional = valorNutricional;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public double getValorNutricional() {
		return valorNutricional;
	}

	public void setValorNutricional(double valorNutricional) {
		this.valorNutricional = valorNutricional;
	}

	public Set<Tamagotchi> getTamagotchis() {
		return tamagotchis;
	}

	public void setTamagotchis(Set<Tamagotchi> herois) {
		this.tamagotchis = herois;
	}

	@Override
	public String toString() {
		return "Aliment [id=" + id + ", nom=" + nom + ", descripcio=" + descripcio + ", valorNutricional="
				+ valorNutricional + ", tamagotchis=" + tamagotchis + "]";
	}

}
