package ActTamagotchi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class MainAct2 {

	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;

	public static void main(String[] args) {

		// Hem d'obrir la sessio fent servir el getSessionFactory();
		session = getSessionFactory().openSession();

		// --------------------------ACTIVITAT 2-------------------------- //

		/**
		 * 
		 * Afegim Tamagotchis i aliments a la BBDD
		 * 
		 */

		session.beginTransaction();

		Tamagotchi tama1 = new Tamagotchi("tama1", "descripcio1");
		Tamagotchi tama2 = new Tamagotchi("tama2", "descripcio2");
		Tamagotchi tama3 = new Tamagotchi("tama3", "descripcio3");
		session.persist(tama1);
		session.persist(tama2);
		session.persist(tama3);

		tama1.setAmics(new ArrayList<Tamagotchi>(Arrays.asList(tama2, tama3)));
		tama2.setAmics(new ArrayList<Tamagotchi>(Arrays.asList(tama3, tama1)));
		tama3.setAmics(new ArrayList<Tamagotchi>(Arrays.asList(tama1, tama2)));

		Aliment poma = new Aliment("poma", "descripcioPoma", 20.00);
		Aliment xocolata = new Aliment("xocolata", "descripcioXocolata", 10.00);
		Aliment pera = new Aliment("pera", "descripcioPera", 15.00);
		Aliment pollastre = new Aliment("pollastre", "descripcioPoma", 30.00);

		session.persist(poma);
		session.persist(xocolata);
		session.persist(pera);
		session.persist(pollastre);

		session.getTransaction().commit();

		/**
		 * 
		 * Recuperem i printem els tamagotchis per veure els seus amics
		 * 
		 */

		@SuppressWarnings("unchecked")
		List<Tamagotchi> tamagotchis1 = session.createQuery("from Tamagotchi").getResultList();
		System.out.println();
		System.out.println("Printem llista de tots els tamagotchis...");
		System.out.println(tamagotchis1);

		/**
		 * 
		 * Afegim tama1, poma Afegim tama2, xocolata Afegim tama3, xocolata
		 * 
		 */

		session.beginTransaction();

		Tamagotchi tama1poma = session.find(Tamagotchi.class, 1);
		Tamagotchi tama2xocolata = session.find(Tamagotchi.class, 2);
		Tamagotchi tama3xocolata = session.find(Tamagotchi.class, 3);

		tama1poma.setAliment(session.find(Aliment.class, 1));
		tama2xocolata.setAliment(session.find(Aliment.class, 2));
		tama3xocolata.setAliment(session.find(Aliment.class, 2));

		System.out.println();
		System.out.println("Tamagotchis amb aliments!");
		System.out.println(tama1poma);
		System.out.println(tama2xocolata);
		System.out.println(tama3xocolata);

		session.merge(tama1poma);
		session.merge(tama2xocolata);
		session.merge(tama3xocolata);
		
		session.getTransaction().commit();

		/**
		 * 
		 * Obtenim tots els registres de tamagotchis i fem print del seu aliment
		 * 
		 */

		System.out.println();
		System.out.println("Registres de tamagotchis amb els seus aliments!");
		@SuppressWarnings("unchecked")
		List<Tamagotchi> elsMeusTamagotchisAmbAliments = session.createQuery("from Tamagotchi").getResultList();
		for (Tamagotchi tamagotchi : elsMeusTamagotchisAmbAliments) {
			System.out.println(tamagotchi.toStringAliment());
		}

		/**
		 * 
		 * Ara genera dos objectes Joguina (els noms han de ser: {“pilota”,
		 * “hotwheels”}) i assigna’ls als Tamagotchi amb ID 1 i 2 respectivament. Fes la
		 * persistencia i desprès modifica la joguina del tamagotchi amb ID 1 per la
		 * joguina del tamagotchi amb ID 2.
		 * 
		 */

		session.beginTransaction();

		Joguina pilota = new Joguina("pilota", "descPilota", 2);
		Joguina hotwheels = new Joguina("hotwheels", "descHotWheels", 39447);

		Tamagotchi tama1joguina = session.find(Tamagotchi.class, 1);
		Tamagotchi tama2joguina = session.find(Tamagotchi.class, 2);

		tama1joguina.setJoguina(pilota);
		tama2joguina.setJoguina(hotwheels);

		session.merge(tama1joguina);
		session.merge(tama2joguina);

		session.getTransaction().commit();

		// fem comprovacio d'aquests últims tamas

		session.beginTransaction();

		Tamagotchi tama1joguinaComp = session.find(Tamagotchi.class, 1);
		Tamagotchi tama2joguinaComp = session.find(Tamagotchi.class, 2);

		System.out.println(tama1joguinaComp);
		System.out.println(tama2joguinaComp);

		// posem al tama1 la joguina del tama2
		// donara error, pq la relacio és 1-1

		// tama1joguinaComp.setJoguina(tama2joguinaComp.getJoguina());

		//
		//
		// esto da ERROR!!!!
		// duplicate ENTRY!
		// funciona BIEN!
		//
		//

		// session.merge(tama1joguinaComp);

		session.getTransaction().commit();

		/**
		 * Consultar tots els tamagotchis de la base de dades i mostrar les dades per
		 * consola. Si en tenen, fes un print del nom de la joguina i l’aliment
		 * assignat.
		 */

		
		@SuppressWarnings("unchecked")
		List<Tamagotchi> elsMeusTamagotchisFinals = session
				.createQuery("from Tamagotchi where id_Aliment is not null and id_Joguina is not null")
				.getResultList();
		System.out.println();
		System.out.println("Imprimim llista amb els tamagotchis que tenen Aliment i Joguina");
		for (Tamagotchi tamagotchi : elsMeusTamagotchisFinals) {
			System.out.println(tamagotchi);
		}
		session.close();
	}

	public static synchronized SessionFactory getSessionFactory() {
		try {
			if (sessionFactory == null) {
				StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
						.configure("hibernate.cfg.xml").build();
				Metadata metaData = new MetadataSources(standardRegistry).getMetadataBuilder().build();
				sessionFactory = metaData.getSessionFactoryBuilder().build();
			}
			return sessionFactory;
		} catch (Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

}
