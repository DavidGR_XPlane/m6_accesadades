package ActTamagotchi;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class MainAct1 {

	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;

	public static void main(String[] args) {

		// Hem d'obrir la sessio fent servir el getSessionFactory();
		session = getSessionFactory().openSession();
		// Totes les operacions a la BBDD s'han de fer amb una transacció (obrim i fem
		// commit)
		session.beginTransaction();

		// --------------------------ACTIVITAT 1-------------------------- //

		/**
		 * 
		 * Afegim dos Tamagotchis a la BBDD
		 * 
		 */

		Tamagotchi tama1 = new Tamagotchi("tama1", "descripcio1");
		Tamagotchi tama2 = new Tamagotchi("tama2", "descripcio2");
		session.persist(tama1);
		session.persist(tama2);
		session.getTransaction().commit();

		/**
		 * 
		 * Recuperem el que te ID 1
		 * 
		 */

		session.beginTransaction();

		Tamagotchi tama1recuperat = session.find(Tamagotchi.class, 1);
		System.out.println();
		System.out.println("Recuperem el primer tamagotchi...");
		System.out.println(tama1recuperat);

		/**
		 * 
		 * Cambiem el nom del Tamagotchi recuperat i comprovem...
		 * 
		 */

		tama1recuperat.setNom("Updategotchi");
		session.merge(tama1recuperat);
		session.getTransaction().commit();

		Tamagotchi tama1updatejat = session.find(Tamagotchi.class, 1);
		System.out.println();
		System.out.println("Print de tamagotchi updatejat...");
		System.out.println(tama1updatejat);

		/**
		 * 
		 * Consultem tots els Tamagotchis i els mostrem per consola...
		 * 
		 */

		session.beginTransaction();

		@SuppressWarnings("unchecked")
		List<Tamagotchi> tamagotchis = session.createQuery("from Tamagotchi").getResultList();
		System.out.println();
		System.out.println("Printem llista de tots els tamagotchis...");
		System.out.println(tamagotchis);
		session.getTransaction().commit();

		session.close();

	}

	public static synchronized SessionFactory getSessionFactory() {
		try {
			if (sessionFactory == null) {
				StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
						.configure("hibernate.cfg.xml").build();
				Metadata metaData = new MetadataSources(standardRegistry).getMetadataBuilder().build();
				sessionFactory = metaData.getSessionFactoryBuilder().build();
			}
			return sessionFactory;
		} catch (Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

}
