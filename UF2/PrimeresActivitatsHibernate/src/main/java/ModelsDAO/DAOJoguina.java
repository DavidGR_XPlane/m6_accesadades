package ModelsDAO;

import ActTamagotchi.Joguina;
import BaseDAO.DAOGeneric;

public class DAOJoguina extends DAOGeneric<Joguina, Integer> {

	public DAOJoguina() {
		super(Joguina.class);
	}

	public void modificarNivellDiversio(int id, int nivellDiversio) {
		Joguina joguina = find(id);
		joguina.setNivellDiversio(nivellDiversio);
		update(joguina);
	}

}
