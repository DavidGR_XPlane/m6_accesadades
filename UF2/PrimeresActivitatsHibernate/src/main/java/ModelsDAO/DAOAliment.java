package ModelsDAO;

import ActTamagotchi.Aliment;
import ActTamagotchi.Tamagotchi;
import BaseDAO.DAOGeneric;

public class DAOAliment extends DAOGeneric<Aliment, Integer> {

	public DAOAliment() {
		super(Aliment.class);
	}

	public void tamagotchisAmbAquestAliment(Aliment alimentDemanat) {
		Aliment a = find(alimentDemanat.getId());
		for (Tamagotchi t : a.getTamagotchis()) {
			
			System.out.println(t.getNom());
		}
		//System.out.println(alimentDemanat.getTamagotchis());
	}

	public void modificarValorNutricional(Aliment aliment, int valorNutricional) {
		aliment.setValorNutricional(aliment.getValorNutricional() + valorNutricional);
	}

}
