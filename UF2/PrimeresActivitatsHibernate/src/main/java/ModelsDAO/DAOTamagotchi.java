package ModelsDAO;

import java.util.List;

import ActTamagotchi.Aliment;
import ActTamagotchi.Joguina;
import ActTamagotchi.Tamagotchi;
import BaseDAO.DAOGeneric;

public class DAOTamagotchi extends DAOGeneric<Tamagotchi, Integer> {

	public DAOTamagotchi() {
		super(Tamagotchi.class);
	}

	public void equiparJoguinaPK(int id, Joguina joguinaEquipar) {
		Tamagotchi tama = find(id);
		tama.setJoguina(joguinaEquipar);
		update(tama);
	}

	public void equiparAlimentPK(int id, Aliment aliment) {
		Tamagotchi tama = find(id);
		tama.setAliment(aliment);
		update(tama);
	}

	public void esticViu(Tamagotchi tama) {
		if (tama.isViu()) {
			System.out.println(tama.getNom() + " està viu!");
			tama.setViu(true);
		} else {
			System.out.println(tama.getNom() + " ha mort :(");
			tama.setViu(false);
		}
		update(tama);
	}

	public void equiparJoguina(Tamagotchi tama, Joguina joguina) {
		tama.setJoguina(joguina);
		update(tama);
	}

	public void equiparAliment(Tamagotchi tama, Aliment aliment) {
		tama.setAliment(aliment);
		update(tama);
	}

	public void jugar(Tamagotchi tama) {
		System.out.println("estic jugant!");
		if (tama.getJoguina() != null) {
			tama.setFelicitat(tama.getFelicitat() + tama.getJoguina().getNivellDiversio());
			System.out.println(tama.getFelicitat());
			tama.setJoguina(null);
		} else {
			System.out.println(tama.getNom() + " no té cap joguina!");
		}
		update(tama);
	}

	public void menjar(Tamagotchi tama) {
		// fem morir per comprobar la seva gana i veure si ha mort
		//morir(tama);
		System.out.println("estic menjant!");
		if (tama.isViu()) {
			if (tama.getAliment() != null) {
				tama.setGana(tama.getGana() - tama.getAliment().getValorNutricional());
				if (tama.getGana() < 0)
					tama.setGana(0);
				tama.setAliment(null);
			} else {
				System.out.println(tama.getNom() + " no té cap aliment!");
			}
			update(tama);
		} else {
			System.out.println(tama.getNom() + " no està viu, per tant, no pot menjar!");
		}

	}

	public void ferAmics(Tamagotchi tama1, Tamagotchi tama2) {
		List<Tamagotchi> tama1amics = tama1.getAmics();
		tama1amics.add(tama2);
		List<Tamagotchi> tama2amics = tama2.getAmics();
		tama2amics.add(tama1);
		update(tama1);
		update(tama2);
		System.out.println(tama1.getNom() + " ara és amic de " + tama2.getNom());
		System.out.println(tama2.getNom() + " ara és amic de " + tama1.getNom());
	}

	public void llistarAmics(Tamagotchi tama1) {
		System.out.println(tama1.getAmics());
	}

	public void morir(Tamagotchi tama) {
		if (tama.getGana() < 200) {
			if (tama.getAliment() != null) {
				this.menjar(tama);
			}
		} else {
			tama.setViu(false);
		}
		update(tama);
	}

}
